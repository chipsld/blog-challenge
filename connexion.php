<?php
$host = 'localhost';
$dbname = 'blog';
$username = 'superblog';
$password = 'root';


// Initialise une connexion a la base de donnee a l'aide de PDO
try {
    $connexion = new PDO("mysql:host=$host;dbname=$dbname", $username, $password);
} catch (PDOException $e) {

    echo ($e->getMessage());
}
