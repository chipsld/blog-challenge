<?php include("header.html"); ?>

<body>
  <?php include("navbar.html"); ?>
  <table class="table">
    <thead>
      <tr>
        <th scope="col">Id</th>
        <th scope="col">Title</th>
        <th scope="col">Content</th>
        <th scope="col">Status</th>
        <th scope="col">Created</th>
        <th scope="col">Updated</th>
      </tr>
    </thead>
    <tbody>
      <?php
      require('./models/article.php');
      foreach (Article::get() as $article) {
        echo ('<tr>
            <th scope="row" id="id">' . $article['Id'] . '</th>
            <td>' . $article['Title'] . '</td>
            <td>' . $article['Content']. '</td>
            <td>' . $article['Statut']. '</td>
            <td>' . $article['Created'] . '</td>
            <td>' . $article['Updated'] . '</td>
            <td> <a href="admin/delete/' . $article['Id']. '" class="btn btn-danger"><i class="fas fa-trash-alt"></i></a> </td>
            <td> <a href="admin/update/' . $article['Id'] . '" class="btn btn-warning"><i class="fas fa-pencil-alt"></i></a> </td>
          </tr>');
      }
      ?>
    </tbody>
  </table>
</body>
<?php include("script.html"); ?>
</html>