<?php include("header.html"); ?>
<body>
    <?php include("navbar.html"); ?>


    <div class="container">
        <?php
        $html = '';
        $html .= ' <h1 class="justify-content-center">' . $article['Title'] . '</h1><br>';
        $html .= '<div class="row">';
        $html .= ' <h5 style="margin-right : 2%"> Created : ' . $article['Created'] . '</h5><br>';
        $html .= '</div';

        $html .= '<div class="row">';
        $html .= ' <h5> Updated :' . $article['Updated'] . '</h5><br>';
        $html .= '</div>';
        $html .= '<div class="row">';
        $html .= ' <h3>' . $article['Content'] . '</h3>';
        $html .= '</div>';
        echo $html;
        ?>

    </div>
</body>
<?php include("script.html"); ?>

</html>