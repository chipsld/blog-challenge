<?php include("header.html"); ?>

<body>
  <?php include("navbar.html"); ?>

  <div class="container">
    <h1> Créer un article</h1>

    <form action="/new" method="POST">
      <div class="form-group">
        <label for="titleArticle"></label>
        <input type="text" class="form-control" id="titleArticle" name="title" placeholder="Titre de l'article">
      </div>
      <div class="form-group">
        <label for="statusArticle">Example select</label>
        <select class="form-control" id="statusArticle" name="status">
          <option value="draft">Draft</option>
          <option value="publish">Publish</option>
        </select>
      </div>
      <div class="form-group">
        <label for="contentArticle">Contenu de l'article</label>
        <textarea class="form-control" id="contentArticle" name="content" rows="12"></textarea>
      </div>

      <button class="btn btn-primary" type="submit">Créer</button>
    </form>
  </div>
</body>
<?php include("script.html"); ?>

</html>