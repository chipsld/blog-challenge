<?php include("header.html"); ?>

<body>
    <?php include("navbar.html"); ?>

    <div class="container">
        <h1> Modifier un article</h1>

        <form action="/change" method="POST">
            <div class="form-group">
                <label for="titleArticle"> Titre de l'article</label>
                <?php
                echo '<input hidden name="id" value="' . $article['Id'] . '">';
                echo '<input type="text" class="form-control" id="titleArticle" name="title" value="' . $article['Title'] . '">'
                ?>
            </div>
            <div class="form-group">
                <label for="statusArticle">Statut de l'article </label>
                <select class="form-control" id="statusArticle" name="status">
                    <?php
                    echo ($article['Statut']);
                    if ($article['Statut'] == 'draft') {
                        echo '<option selected value="draft">Draft</option>';
                        echo '<option value="publish">Publish</option>';
                    } else {
                        echo '<option selected value="publish">Publish</option>';
                        echo '<option  value="draft">Draft</option>';
                    }

                    ?>

                </select>
            </div>
            <div class="form-group">
                <label for="contentArticle">Contenu de l'article</label>
                <?php
                echo ('<textarea class="form-control" id="contentArticle" name="content" rows="12">' . $article['Content'] . '</textarea>')
                ?>
            </div>

            <button class="btn btn-primary" type="submit">Modifier</button>
        </form>
    </div>
</body>
<?php include("script.html"); ?>

</html>