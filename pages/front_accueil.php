<?php include("header.html"); ?>
<body>
    <?php include("navbar.html"); ?>
    <div class="container">
        <div class="row">
            <h1>blog-challenge</h1>
        </div>
        <div class="row">
        <?php
          require('./models/article.php');

              foreach (Article::get() as $article) {
                  if($article['Statut'] == 'publish')
                  {
                    echo('
                        <div class="card" style="width: 18rem;">
                          <div class="card-body">
                            <h5 class="card-title">' . $article['Title'] . '</h5>
                            <h6 class="card-subtitle mb-2 text-muted">' . $article['Created']. '</h6>
                            <p class="card-text">' . $article['Content']. '</p>
                            <a href="/front/' . $article['Id']. '" class="card-link"> Lien de l\'article </a>
                          </div>
                        </div>
                    ');
                  }
              }
        ?>
        </div>
    </div>
</body>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
</html>