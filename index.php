<?php

require('./src/autoloader.php');

// on stock l'url appellee  et on apelle une instance de la classe Router en donnant l'url en paramatre
$url = $_SERVER['REQUEST_URI'];

$router = new Router($url);

$router->get('/front', function () {
    require('./pages/front_accueil.php');
});

$router->get('/front/:id', function ($id_article) {
    require_once('./models/article.php');
    $article = Article::getOne($id_article);
    require('./pages/front_article.php');
});

$router->get('/admin/', function () {
    require('./pages/front_admin.php');
});

$router->post('/admin/create', function () {
    require('./pages/front_create.php');
});

$router->post('admin/delete/:id', function ($id) {
    require_once('./models/article.php');
    Article::delete($id);
});

$router->post('/admin/update/:id', function ($id_article) {
    require_once('./models/article.php');
    $article = Article::getOne($id_article);
    require('./pages/front_update.php');
});

$router->post('/new', function () {

    require_once('./models/article.php');
    $title = $_POST['title'];
    $status = $_POST['status'];
    $content = $_POST['content'];

    $article = new Article($title,$content,$status);

    Article::create($article);
});

$router->post('/change', function () {

    require_once('./models/article.php');

    $title = $_POST['title'];
    $status = $_POST['status'];
    $content = $_POST['content'];

    $article = new Article($title,$content,$status);

    Article::update($article);
});


$router->get('/admin', function () {
    require('./pages/admin.php');
});

$router->run();
