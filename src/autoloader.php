<?php

// Cette classe est appele au debut de index.php afin de charger les classes route et router
class Autoloader
{
    public static function register()
    {
        spl_autoload_register(function ($class) {

            $file = 'src/' . $class . '.php';
            // Si un fichier du nom de la classe existe alors on include ce fichier dans index.php
            if (file_exists($file)) {
                include $file;
            }
        });
    }
}

Autoloader::register();
