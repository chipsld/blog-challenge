<?php
class Route
{

    private $path;
    private $callable;
    private $matches = [];
    private $params = [];

    public function __construct($path, $callable)
    {
        $this->path = trim($path, '/'); // On retire les / inutiles
        $this->callable = $callable;
    }

    /**
     * Permettra de capturer l'url avec les paramètre
     * get('/posts/:id') par exemple
     **/
    public function match($url)
    {

        $url = trim($url, '/');

        // Si la route possede 2 parametres
        if (explode('/', $url) == 2) {
            $regex = "^(?:\/\b)(?:\/[\w]+){2}$";
            // On verifie que la route corresponde a l'expression reguliere
            if (!preg_match($regex, $url, $matches)) {
                return false;
            }
        } else {
            // On remplace les : de la route par un /
            $path = preg_replace('#:([\w+]+)#', '([^\/]+)', $this->path);
            $regex = "#^$path$#";

            // on regarde si l'url verifie l'expression reguliere compose avec la route
            if (!preg_match($regex, $url, $matches)) {
                return false;
            }
        }

        // On extrait la premiere valeur du tableau matches et on le stock dans le tableau de tout les matchs
        array_shift($matches);
        $this->matches = $matches;
        return true;
    }

    public function call()
    {
        // Retourne sous forme de tableau les parametres de la route
        return call_user_func_array($this->callable, $this->matches);
    }
}
