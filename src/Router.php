<?php

class Router
{
    private $url;
    private $routes = [];

    public function __construct($url)
    {
        $this->url = $url;
    }

    /**
     * Créer une instance de Route
     * Stock cet objet dans le tableau des routes
     **/

    public function post($path, $callable)
    {
        $route = new Route($path, $callable);
        $this->routes['POST'][] = $route;
    }

    /**
     * Créer une instance de Route
     * Stock cet objet dans le tableau des routes
     **/

    public function get($path, $callable)
    {
        $route = new Route($path, $callable);
        $this->routes['GET'][] = $route;
    }



    public function run()
    {
        /** Si le tableau ne contient pas la methode on throw une erreur  */
        if (!isset($this->routes[$_SERVER['REQUEST_METHOD']])) {
            throw new Exception('Pas de route correspondante');
        }


        // On scan l'ensemble des routes  stocke dans le tableau
        foreach ($this->routes['GET'] as $route) {
            // Si la route correspond a l'url on execute la fonction call
            if ($route->match($this->url)) {
                return $route->call();
            }
        };

        foreach ($this->routes['POST'] as $route) {
            if ($route->match($this->url)) {
                return $route->call();
            }
        };
    }
}
