<?php
require('connexion.php');

$table = "article";

// Requete insert de la table article dans la base de donnee
$createQuery = "CREATE table $table(
        
    Id INT(3) AUTO_INCREMENT PRIMARY KEY,
    Title VARCHAR(250) NOT NULL, 
    Content TEXT NOT NULL,
    Statut ENUM('publish','draft'), 
    Created DATE, 
    Updated DATE)";


// Si l'execution de la reauete ne retourne pas d'erreur on affiche que la table a ete cree
// sinon on renvoi l'erreur
try {
    $connexion->exec($createQuery);
    echo ("Created $table Table.\n");
} catch (PDOException $e) {
    echo $e->getMessage(); //Remove or change message in production code
}
