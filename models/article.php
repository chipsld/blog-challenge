<?php

class article
{
    private $title;
    private $content;
    private $status;
    private $created;
    private $updated;


    public function __construct($title, $content, $status)
    {
        $this->title = $title;
        $this->content = $content;
        $this->status = $status;
        $this->created = date('Y-m-d');
        $this->updated = '';
    }

    public static function create($article)
    {
        require('./connexion.php');

        $insertArticle = "INSERT into article (Title,Content,Statut,Created,Updated)
            VALUES
            ('$article->title','$article->content','$article->status','$article->created','')";
        try {
            $connexion->query($insertArticle);
            header('Location: /admin');
        } catch (PDOException $e) {
            echo $e->getMessage(); //Remove or change message in production code
        }
    }

    public static function get()
    {
        require('./connexion.php');
        $getArticle = "select * from article";


        try {
            $allArticle = $connexion->query($getArticle);
            $articles = $allArticle->fetchAll();

            return $articles;
        } catch (PDOException $e) {
            echo $e->getMessage(); //Remove or change message in production code
        }
    }


    public static function getOne($id_article)
    {
       require('./connexion.php');
       $getOneArticle = "select * from article where id =" . $id_article;

           try {
               $oneArticle = $connexion->query($getOneArticle);
               $article = $oneArticle->fetch();

               return $article;
           } catch (PDOException $e) {
               echo $e->getMessage(); //Remove or change message in production code
           }
    }

     public static function delete($id_article)
        {
            require('./connexion.php');

            $deleteArticle = "delete from article where id =" . $id_article;
            try {
                $connexion->query($deleteArticle);
                 header('Location: /admin');
            } catch (PDOException $e) {
                echo $e->getMessage(); //Remove or change message in production code
            }

        }

        public static function update($new_article)
        {
            require('./connexion.php');
            $id_article = $_POST['id'];

            $updated = date('Y-m-d');

            $updateArticle = "update article set
            Title =" . '\'' . $new_article->title . '\'' . ",
            Content = " . '\'' . $new_article->content . '\'' . ",
            Statut =" . '\'' . $new_article->status . '\'' . ",
            Updated = " . '\'' . $updated . '\'' . "
            where Id =" . $id_article;

            try {
                $connexion->query($updateArticle);
                header('Location: /admin');
            } catch (PDOException $e) {
                echo $e->getMessage(); //Remove or change message in production code
            }

        }
}
